package ru.innopolis.stc9.lesson16;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SearchBase implements Occurencies {
    final static Logger logger = Logger.getLogger(SearchBase.class);
    private static final int PART_SIZE = 10_000_000;
    private static int fileIndex = 0;
    private static int skipStart = 0;
    private static int stopPos = PART_SIZE;
    boolean thisFileEnds = false;


    String[] sources;
    String[] words;
    String res;

    @Override
    public void getOccurencies(String[] sources, String[] words, String res) {
        this.sources = sources;
        this.words = words;
        this.res = res;


        long start = System.currentTimeMillis();

        logger.info("Ожидание завершения потоков...");
        int nThreads = 100;
        ExecutorService executorService = Executors.newFixedThreadPool(nThreads);

        for (int i = 0; i < nThreads; i++) {
            executorService.submit(new Searcher(this));
        }

        executorService.shutdown();

        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
            logger.info("Все потоки завершены.");
            long now = System.currentTimeMillis();
            int delta = (int) (now - start) / 1000;
            logger.info("Длительность: " + delta + " сек.");
        } catch (InterruptedException e) {

        }
    }

    public synchronized String getNextSource() throws IOException {
            if (fileIndex < sources.length) {
                String filePath = sources[fileIndex];
                String filePart = getFileTextPart(filePath);

                return filePart;
            } else {
                return null;
            }
    }

    public synchronized String getFileTextPart(String filePath) throws IOException {
            File file = new File(filePath);
            InputStream is = new FileInputStream(file);

            int available = is.available();

            int len = getAvaliableLength(available);

            int offset = 0;
            int numRead = 0;
            byte[] filePart = new byte[len];

            is.skip(skipStart);

            while (offset < len && (numRead = is.read(filePart, 0, len)) >= 0) {
                offset += numRead;
            }

            is.close();

            StringBuilder filePartStringBuilder = new StringBuilder(new String(filePart, "UTF-8"));
            String filePartString;
            int lastPoint = filePartStringBuilder.lastIndexOf(".") + 1;
            skipStart += lastPoint;
            stopPos = skipStart + PART_SIZE;
            filePartString = filePartStringBuilder.substring(0, lastPoint).trim();

            if (thisFileEnds) {
                skipStart = 0;
                stopPos = PART_SIZE;
                fileIndex++;
                int fileIndexToShow = fileIndex + 1;
                logger.info("(" + fileIndexToShow + "/" + sources.length + ") Считываю новый файл: "
                        + sources[fileIndex]);
            }

            return filePartString;
    }

    public int getAvaliableLength(int available){

        int len = 0;
        thisFileEnds = false;
        if (available < stopPos) {
            len = available - skipStart;
            thisFileEnds = true;
        } else {
            len = stopPos - skipStart;
        }

        return len;
    }

    public static int getSkipStart() {
        return skipStart;
    }

    public static void setSkipStart(int skipStart) {
        SearchBase.skipStart = skipStart;
    }

    public static int getStopPos() {
        return stopPos;
    }

    public static void setStopPos(int stopPos) {
        SearchBase.stopPos = stopPos;
    }
}
