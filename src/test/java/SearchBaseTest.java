import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import ru.innopolis.stc9.lesson16.SearchBase;
import java.io.IOException;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import static junit.framework.Assert.*;

public class SearchBaseTest {
    private static Logger logger = Logger.getLogger(SearchBaseTest.class);
    private SearchBase searchBase;

    @Before
    public void before(){
        this.searchBase = new SearchBase();
    }

    @Test
    public void getAvaliableLengthTest(){
        this.searchBase.setSkipStart(300);
        this.searchBase.setStopPos(40000);
        int len1 = this.searchBase.getAvaliableLength(50000);

        assertTrue("Length1 is wrong", len1 == 39700);

        this.searchBase.setStopPos(56000);
        int len2 = this.searchBase.getAvaliableLength(50000);
        assertTrue("Length2 is wrong", len2 == 49700);
    }
}
